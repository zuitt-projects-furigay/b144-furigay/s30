const express = require("express");
// Mongoose is a package that allos creation of schema to model our data structures and to have an access to a number of methods for manipuulating

const mongoose = require("mongoose")
const app = express();
const port = 3001;

// --------------------------

// MongoDB Connection
// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://adriannfurigay:qwerty123@wdc028-course-booking.umyxd.mongodb.net/batch144-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

// Set notifications for connection success or failure
let db = mongoose.connection;
// if error occured, output in the console
db.on("Error", console.error.bind(console, "connection error"))

// if the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud datababse"))

app.use(express.json())
app.use(express.urlencoded({extended:true}))

// -------------------------



//Mongoose Schemas
// Schema determine the structure of the documnents to be written in the datbase
// it acts as a blueprint to our data
// we will use schema() constructor of the mongoose module to create a new schema object.

// BLUEPRINT
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		// default values are the predefined values for a field if we dont put any value
		default: "pending"
	}
})

// if naming a model it should be singular and Capitalized
const Task = mongoose.model("Task", taskSchema)
// models are what allows us to gain access to methods that will perform CRUD functions.
// Models must be in singular form and capitalized following the MVC approach for naming convention.
// first parameter of the mongoos model method indicates the collection in where to store the data
// second param is used to specify the schema/blueprint.



// ROUTES

// Create a new task
/*
Business Logic

1.	Add a functionality to check if there are duplicate task
		-if the tast already exist, return there is a duplicate
		-if the task doesnt  exist, we can add it in the database
2.	The task data will be coming from the request's body
3.	Create new Task object with properties that we need
4.	Then save the data

*/

// POST
app.post("/tasks", (req, res) =>{
	// Check if there are duplicate
	// findOne() is a mongoose method that acts similar to "fiind".
	// it returns the first document that matches thhe search criteria
	Task.findOne( {name: req.body.name}, (error, result) => {
		// if a document was found and the documnent's name matches the information sent cia clientpostman
		if(result !== null && result.name == req.body.name){
			// return a mmessage to a client/postman
			return res.send("Duplicate task found!")
		}else{
			// if no document was found
			// create a new task and save it to the database
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save((saveErr, savedTask) => {
				// if there are errors in saving 
				if(saveErr){
					// will print any error found in the console
					// saveErr is an error object that will contain details about the error
					// Errors normally come as an object data type
					return console.error(saveErr)
				}else{
					// no error found while creating the document
					// Return a sttatus code of 201 for successful cretion
					return res.status(201).send("New Task Created")
				}
			})
		}



	} )
})


/*
RETRIEVING ALL DATA
BUSINESS LOGIC 

1. Retrieve all the documennts using the find().
2. If an error is encountered, print the error.
3. If no errors are found, send a success status back to the client and return an array of documents.

*/

app.get("/tasks", (req, res) => {
	// An empty "{}" means it retirns all the documents and stores them int he "result" parameter of the call back function.
	Task.find( {}, (err, result) => {
		// If error occured
		if(err){
			return console.log(err)
		} else {
			return res.status(200).json({
				data: result
			})
		}
	} )
})


/*
REGISTER 
BUSINESS LOGIC

1. Find if there are duplicate user
	-if user already exists, return an error
	-if user doesn't exist, we add it in the database
		-if the username and password are both not blank
			-if blannk, send responnse "BOTH username and password must be provided"
			-both condiition has been met, create a new object.
			-Save the new object
				-if error, return error message
				-else, response a status 201 and "New User Registered"

*/



const userSchema = new mongoose.Schema({
	username: String,
	password: String
	
})

const User = mongoose.model("User", userSchema)


app.post("/users", (req, res) =>{

	User.findOne( {username: req.body.username}, (error, result) => {
		
		if(result !== null && result.username == req.body.username){
		
			return res.send("Username already exist")
		}else{

			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save((saveErr, savedTask) => {
			
				if(saveErr){
				
					return console.error(saveErr)
				}else{
			
					return res.status(201).send("New User Registered")
				}
			})
		}



	} )
})


app.get("/users", (req, res) => {

	User.find( {}, (err, result) => {
		if(err){
			return console.log(err)
		} else {
			return res.status(200).json({
				data: result
			})
		}
	} )
})



app.listen(port, () => console.log(`Server is running at port ${port}.`))